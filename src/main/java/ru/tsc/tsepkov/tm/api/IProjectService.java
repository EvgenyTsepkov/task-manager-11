package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);
    
}
