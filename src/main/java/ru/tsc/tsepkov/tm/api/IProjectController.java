package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Project;

public interface IProjectController {

    void createProject();

    void showProjects();

    void showProjectById();

    void showProject(Project project);

    void showProjectByIndex();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
