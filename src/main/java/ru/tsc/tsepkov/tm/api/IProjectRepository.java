package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Project;
import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    void clear();

}
