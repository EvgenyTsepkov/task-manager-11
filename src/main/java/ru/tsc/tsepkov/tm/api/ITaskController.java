package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Task;

public interface ITaskController {

    void createTask();

    void showTasks();

    void showTaskById();

    void showTask(Task task);

    void showTaskByIndex();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();
    
}
